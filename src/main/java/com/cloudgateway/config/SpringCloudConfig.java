package com.cloudgateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {
	
	@Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
        		 .route(r -> r.path("/auth/**")
                         .uri("http://localhost:2000/")
                        )
        		 .route(r -> r.path("/microservice1/**")
                         .uri("http://localhost:8082/")
                        )
        		 .route(r -> r.path("/microservice2/**")
                         .uri("http://localhost:4000/")
                        )
        		 .route(r -> r.path("/microservice3/**")
                         .uri("http://localhost:8083/")
                        )
        		 .route(r -> r.path("/microservice4/**")
                         .uri("http://localhost:4001/")
                        )
                 .build();
                        
    }

}
